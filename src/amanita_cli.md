# Amanita CLI - управляющий интрерфейс
## Введение
Данная утилита создана для простого и удобного разворачивания и управления компонентами Amanita Telefonica.

Подробная и исчерпывающая справочная информация встроена в утилиту и доступна для любого уровня при 
использовании опции `--help`. Например:

```text
amanita --help
```
```text
Usage: amanita [OPTIONS] COMMAND [ARGS]...

  Amanita Telefonica Command Center.  Here you manage Amanita services
  deployed on current machine. Please select your command....

Options:
  --help  Show this message and exit.

Commands:
  asterisk         Asterisk PBX management
  asterisk-broker  Asterisk PBX Broker management
  config           Configuration management
  console-broker   Asterisk console helper management
  odoo             Odoo Management
  odoo-addons      Amanita Odoo Addons install / update / remove
  odoo-broker      Amanita Odoo Broker management
  postgres         PostgreSQL management
  rabbitmq         RabbitMQ management
  spawn            Amanita Telefonica spawn
  supervisor       Services management
  test             Test utilities

```
Справочную информацию по любому разделу можно получить применив к нему опцию `--help`.
Например:

```sh
amanita asterisk --help
```

## Управление компонентами
Все компоненты могут быть развернуты как на одном сервере (что в принципе не желательно, как
минимум Asterisk рекомендуется выносить на отдельный сервер), так и на разных серверах.

В случае использования нескольких серверов утилита `amanita-cli` должна быть установлена на каждый из них,
и далее на каждом сервере разворачивается свой набор компонентов.
В общем компоненты решения можно разделить на 2 группы:

* Системные
* Собственные

### Системные компоненты
Это:

* PostgreSQL
* RabbitMQ сервер
* Asterisk
* Supervisor

Управляющая утилита Amanita CLI осуществляет *только* уставку системных компонентов из
системных пакетов (apt для Ubuntu / Debian и yum для CentOs). При установке данные компоненты
прописываются в автозагрузку systemd и запускаются автоматически при запуске Системы.

### Собственные компоненты
Собственные компоненты устанавливаются в пути, указанном в `AMANITA_PREFIX` (значение по-умолчанию: 
`/srv/amanita`). 


## Особые режимы запуска
### amanita spawn start
Данный режим предназначен для автоматической установки всех компонентов на один сервер в рамках знакомства
с продуктом. Для production deploy рекомендуется как минимум отдельный сервер только для Asterisk, и пошаговое
разворачивание компонентов из соответсвтующих разделов.

### FORCE_START=1
При указании переменной окружения  FORCE_START=1 при установке системных компонентов выполняется
"ручной запуск" системных служб, например `/etc/init.d/postgres start`. Это сделано в целях тестирования с
использованием docker контейнеров, которые не содержат службы systemd, поэтому системные сервисы необходимо
поднимать "вручную".

## Расширение функционала Amanita CLI
 Управляющая утилита построена на базе принципов, которые делают расширение функционала
 очень простым делом. В качестве примера расширения функционала мы рассмотрим установку
 пакета asterisk-prompt-ru и создание раздела для вывода информации о текущих звонках.

Данный функционал можно добавить в раздел asterisk, но в целях обучения это будет сделано в отдельном разделе.

### Создаем раздел верхнего уровня asterisk-ru
Чуть выше мы уже склонировали репозитарий Amanita CLI в домашнюю папку.
Создаем новый раздел в виде папки.
```text
cd ~/cli/
mkdir amanita_cli/asterisk_ru
echo 'Asterisk RU Management' > amanita_cli/asterisk_ru/.module
```
В данном примере была создана новая папка `asterisk_ru`, в которую помещен файл `.module` с 
описанием модуля. Этого достаточно для того, чтобы в главном меню `amanita` появился новый раздел `asterisk-ru`.

### Добавляем сценарий установки
Утилита amanita спроектирована таким образом, что все файлы с расширением .yml 
(кроме начинающихся на `_`) становятся командами соотвествующего раздела.

Создаем файл `install.yml` следующего содержания:
```yml
---
- hosts: 127.0.0.1
  connection: local

  tasks:
    - name: Install Asterisk RU sounds
      apt:
        name: asterisk-prompt-ru
```
После добавления данного файла можно выполнить команду `amanita asterisk-ru install`, 
которая выполнит этот сценарий.

### Добавляем вывод текущих каналов
Создаем файл `amanita_cli/asterisk_ru/cmd_channels.py` со следующим содержанием:
```python
import click
import subprocess
from amanita_cli.main import config_load

@click.command(help='Show current calls')
def cli():
    config = config_load()
    asterisk_binary = config['ASTERISK_BINARY']
    try:
        out = subprocess.check_output(' '.join(
            [asterisk_binary, '-rx', '"core show channels"']), shell=True)
        click.echo(out.decode())
    except subprocess.CalledProcessError as e:
        click.secho(e.output.decode(), bold=True, fg='red', bg='black')
```
Таким образом, все файлы с расширением `.py`, и которые начиаются на `cmd_`, становятся
командами Amanita CLI.



