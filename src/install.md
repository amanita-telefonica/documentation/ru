# Установка и запуск Amanita Telefonica
Реализовано два способа запуска продукта Amanita Telefonica:
* Docker-compose деплой.
* При помощи утилиты Amanita CLI.

## Docker compose deploy

## Amanita CLI
### Системные требования
Требования для работы утилиты Amanita CLI:
* Python3 и менеджер пакетов pip3
* Ansible
* Git

Перед установкой `Amanita CLI` необходимо установить вышеперечисленные зависимости.

Пример для Ubuntu / Debian
```
apt install -y python3 python3-pip git
```

Установка осуществляется следующей строкой
```sh
pip3 install ansible amanita-cli
```

## Установка версии для разработчиков

```sh
pip3 install --upgrade setuptools
pip3 install -e cli/
```

## Ошибки установки
### Ошибки локализации
Данная ошибка имеет следующее проявление:
```sh
2019-11-07 04:09:06,376 26831 INFO ? odoo: database: odoo@default:default
2019-11-07 04:09:06,404 26831 ERROR ? odoo.sql_db: bad query: CREATE DATABASE "amanita" ENCODING 'unicode' LC_COLLATE 'C' TEMPLATE "template0"
ERROR: encoding "UTF8" does not match locale "en_US"
DETAIL:  The chosen LC_CTYPE setting requires encoding "LATIN1".

Traceback (most recent call last):
  File "/usr/local/bin/odoo", line 8, in <module>
    odoo.cli.main()
  File "/usr/local/lib/python3.6/dist-packages/odoo/cli/command.py", line 60, in main
    o.run(args)
  File "/usr/local/lib/python3.6/dist-packages/odoo/cli/server.py", line 176, in run
    main(args)
  File "/usr/local/lib/python3.6/dist-packages/odoo/cli/server.py", line 140, in main
    odoo.service.db._create_empty_database(db_name)
  File "/usr/local/lib/python3.6/dist-packages/odoo/service/db.py", line 108, in _create_empty_database
    (name, collate, chosen_template)
  File "/usr/local/lib/python3.6/dist-packages/odoo/sql_db.py", line 148, in wrapper
    return f(self, *args, **kwargs)
  File "/usr/local/lib/python3.6/dist-packages/odoo/sql_db.py", line 225, in execute
    res = self._obj.execute(query, params)
psycopg2.DataError: encoding "UTF8" does not match locale "en_US"
DETAIL:  The chosen LC_CTYPE setting requires encoding "LATIN1".
```
Это означает, что на сервере перед началом установки не была сконфигурирована локализация с поддержкой UTF-8.
Чтобы решить данную проблему, необходимо:

* Настроить системную локализацию.
* Удалить кластер базы данных PostgreSQL и установить его заново.

#### Настройка локализации
Отредактируйте файл `/etc/default/locale`, чтобы он имел слудюущее содержание:
```sh
LANG=en_US.UTF-8
LC_ALL=en_US.UTF-8
```
Сгенерируйте файлы локализации:
```sh
root@demo:~ locale-gen en_US.UTF-8
Generating locales (this might take a while)...
  en_US.UTF-8... done
Generation complete.
root@demo:~
```
Перестановите переменные окружения:
```sh
update-locale LANG=en_US.utf8
```
#### Переинициализация базы

```sh
root@demo:~ pg_lsclusters
Ver Cluster Port Status Owner    Data directory              Log file
10  main    5432 online postgres /var/lib/postgresql/10/main /var/log/postgresql/postgresql-10-main.log
```

```sh
root@demo:~ systemctl stop postgresql@11-main
```

```sh
pg_dropcluster --stop 110 main
pg_createcluster --start 10 main
```
